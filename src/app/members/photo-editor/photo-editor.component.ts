import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Photo } from "../../_models/Photo";
import { FileUploader } from "ng2-file-upload";
import { environment } from "../../../environments/environment";
import { AuthService } from "../../_services/auth.service";
import { UserService } from "../../_services/user.service";
import { AlertifyService } from "../../_services/alertify.service";
import * as _ from "underscore";


@Component({
  selector: "app-photo-editor",
  templateUrl: "./photo-editor.component.html",
  styleUrls: ["./photo-editor.component.css"]
})
export class PhotoEditorComponent implements OnInit {
  @Input() photos: Photo[];
  uploader: FileUploader = new FileUploader({});
  hasBaseDropZoneOver = false;
  baseUrl = environment.apiUrl;
  currentMain: Photo;
  // we're gonna pass a string into the event emitter because it's gonna be a URL
  @Output() getMemberPhotoChange = new EventEmitter<string>();

  constructor(
    private authServie: AuthService,
    private userService: UserService,
    private alertify: AlertifyService
  ) {}

  ngOnInit() {
    this.initializeUploader();
  }

  // this provide the functionality to drag and drop file
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  initializeUploader() {
    this.uploader = new FileUploader({
      url:
        this.baseUrl +
        "users/" +
        this.authServie.decodedToken.nameid +
        "/photos",
      authToken: "Bearer " + localStorage.getItem("token"),
      isHTML5: true,
      allowedFileType: ["image"],
      removeAfterUpload: true,
      autoUpload: false,
      maxFileSize: 10 * 1024 * 1024 // this is pretty much like 10 mb
    });

    this.uploader.onSuccessItem = (item, response, status, headers) => {
      if (response) {
        const res: Photo = JSON.parse(response);
        const photo = {
          id: res.id,
          url: res.url,
          dateAdded: res.dateAdded,
          description: res.description,
          isMain: res.isMain
        };
        this.photos.push(photo);
        if (photo.isMain) {
          this.authServie.changeMemberPhoto(photo.url);
          this.authServie.currentUser.photoUrl = photo.url;
          localStorage.setItem('user', JSON.stringify(this.authServie.currentUser));
        }
      }
    };
  }

  setMainPhoto(photo: Photo) {
    this.userService
      .setMainPhoto(this.authServie.decodedToken.nameid, photo.id)
      .subscribe(
        () => {
          // here what happens is first find the photo which was the current main photo using the
          // underscore library
          // then set that found photo to false
          // and finally set the newly current photo to true --pretty easy
          this.currentMain = _.findWhere(this.photos, { isMain: true });
          this.currentMain.isMain = false;
          photo.isMain = true;
          this.authServie.changeMemberPhoto(photo.url);
          this.authServie.currentUser.photoUrl = photo.url;
          localStorage.setItem('user', JSON.stringify(this.authServie.currentUser));
        },
        error => {
          this.alertify.error(error);
        }
      );
  }

  deletePhoto(id: number) {
    this.alertify.confirm('Are you sure you want to delete this photo', () => {
      this.userService.deletePhoto(this.authServie.decodedToken.nameid, id).subscribe(() => {
        this.photos.splice(_.findIndex(this.photos, {id: id}), 1);
        this.alertify.success('Photo has been deleted');
      }, error => {
        this.alertify.error('Failed to delete photo')
      })
    })
  }
}
